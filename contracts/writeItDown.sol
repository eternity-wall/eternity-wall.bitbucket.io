// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.7.0 <0.9.0;

/**
 * @title WriteItDown
 * @dev Writes something on the blockchain. Forever.
 */
 contract WriteItDown {

    // Address of the latest message. Previous messages can be retrieved from it.
    address LastAddress;

    // Structure representing a message.
    struct Message {
        string text;
        address previous;
    }

    // messages left until now
    mapping(address => Message) public messages;

    function AddMessage(string memory _text) public {
        address messageAddress = address(uint160(uint(keccak256(abi.encodePacked(_text, msg.sender, blockhash(block.number))))));
        messages[messageAddress] = Message(_text, LastAddress);
        LastAddress = messageAddress;
    }

    function RetrieveLastMessage() public view returns (Message memory m) {
        return RetrieveMessageAt(LastAddress);
    }

    function RetrieveMessageAt(address messageAddress) public view returns (Message memory m) {
        return messages[messageAddress];
    }
}
